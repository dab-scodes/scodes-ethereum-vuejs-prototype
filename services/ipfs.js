import IPFS from 'ipfs';

let ipfsObject = {};

ipfsObject.inited = false;

ipfsObject.init = async() => {
  console.log('IPFS: initialising node');
  let node = new IPFS({
    config: {
      Addresses: {
        Swarm: [
        '/dns4/ws-star.discovery.libp2p.io/tcp/443/wss/p2p-websocket-star'
        ]
      }
    }
  });
  return new Promise((resolve) => {
    node.on('ready', async function(test) {
      console.log('IPFS: Node is ready');
      window.ipfs = node;
      ipfsObject.inited = true;
      resolve(node);
    })
  })
}

ipfsObject.getNode = async() => {
  if (ipfsObject.inited) {
    return new Promise((resolve, reject) => {
      resolve(window.ipfs);
    });
  }

  let node = await ipfsObject.init();
  return window.ipfs;
};

// *
//  * add a new post into ipfs
//  * @param  string data The data to be posted
//  * @return {
//  *         path: path of the file
//  *         hash: hash of the file
//  *         url: public url of the file
//  *         }      The file that has been posted
//  *
ipfsObject.add = async(data) => {
  let node = await ipfsObject.getNode();
  const filesAdded = await node.add({
    content: Buffer.from(data)
  })

  let file = {};
  file.path = filesAdded[0].path;
  file.hash = filesAdded[0].hash;
  file.url = 'https://ipfs.io/ipfs/' + filesAdded[0].hash;
  console.log('IPFS: New data created', file);

  return file;
};

// *
//  * read a file on ipfs
//  * @param  {string} hash Hash to read
//  * @return {string} The content to be read
//  *
ipfsObject.read = async(hash) => {
  let node = await ipfsObject.getNode();

  try {
    let fileBuffer = await node.cat(hash)
    let data = fileBuffer.toString();
    console.log('IPFS: data read', hash, data);

    return data;
  } catch (e) {
    console.error('IPFS READ ERROR', e);
    return false;
  }

};

export const IPFS_MANAGER = ipfsObject;
