export const ETHEREUM_READ_WRITE_NETWORK = 3;
export const ETHEREUM_READ_WRITE_ADDRESS = '0xdee3ada00dd24e58877671c90f2b19af616ea39f';
export const ETHEREUM_READ_WRITE_ABI = [
  {
    "constant": false,
    "inputs": [
      {
        "name": "_hash",
        "type": "string"
      }
    ],
    "name": "updateHash",
    "outputs": [
      {
        "name": "",
        "type": "bool"
      }
    ],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "constructor"
  },
  {
    "anonymous": false,
    "inputs": [],
    "name": "savedHash",
    "type": "event"
  },
  {
    "constant": true,
    "inputs": [],
    "name": "created_by",
    "outputs": [
      {
        "name": "",
        "type": "address"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [],
    "name": "currentHash",
    "outputs": [
      {
        "name": "",
        "type": "string"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [],
    "name": "previousHash",
    "outputs": [
      {
        "name": "",
        "type": "string"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [],
    "name": "updated_by",
    "outputs": [
      {
        "name": "",
        "type": "address"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  }
];

export const ETHEREUM_READ_WRITE_SMARTCONTRACT = `
pragma solidity ^0.5.1;
contract Hash {

    string public currentHash;
    string public previousHash;
    address public created_by;
    address public updated_by;

    event savedHash(

        );

    constructor () public {
        //Set created_by to who creates the contract
        created_by = msg.sender;
    }

    function updateHash(string memory _hash) public returns (bool) {
        previousHash = currentHash;
        currentHash = _hash;
        updated_by = msg.sender;
        emit savedHash();
        return true;
    }

}
`;
