import {ETHEREUM_READ_WRITE_SMARTCONTRACT} from './ethereum_read_write'
import {SHARED_TABLE_SMARTCONTRACT} from './shared_table'

let sm = {};
sm.ethereum_read_write = ETHEREUM_READ_WRITE_SMARTCONTRACT;
sm.shared_table = SHARED_TABLE_SMARTCONTRACT;

export default sm;
