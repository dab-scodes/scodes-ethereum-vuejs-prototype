import { WEB3_GET, WEB3_POLL } from '~/services/web3';

export const state = () => ({
  isInjected: false,
  web3Instance: null,
  networkId: null,
  coinbase: null,
  balance: null,
  error: null
});

export const getters = {
  getInstance: state => state,
};


export const mutations = {
  registerWeb3Instance (state, payload) {
    console.log('WEB3_STORE: registerWeb3Instance Mutation being executed', state, payload)
    let result = payload
    let web3Copy = state
    web3Copy.coinbase = result.coinbase
    web3Copy.networkId = result.networkId
    web3Copy.balance = parseInt(result.balance, 10)
    web3Copy.isInjected = result.injectedWeb3
    web3Copy.web3Instance = result.web3
    state = web3Copy;
    WEB3_POLL(this);
  },
  pollWeb3Instance (state, payload) {
    console.log('pollWeb3Instance mutation being executed', payload)
    state.coinbase = payload.coinbase
    state.balance = parseInt(payload.balance, 10)
  },
};

export const actions = {
  async registerWeb3 ({commit}) {
    console.log('WEB3_STORE: registerWeb3 Action being executed')
    WEB3_GET().then(result => {
      console.log('WEB3_STORE: committing result to registerWeb3Instance mutation')
      commit('registerWeb3Instance', result)
    }).catch(e => {
      console.error('WEB3_STORE: error in action registerWeb3', e)
    })
  },
  pollWeb3 ({commit}, payload) {
    console.log('WEB3_STORE: pollWeb3 action being executed')
    commit('pollWeb3Instance', payload)
  },
};
