import Web3 from 'web3'
import {ETHEREUM_READ_WRITE_NETWORK, ETHEREUM_READ_WRITE_ADDRESS, ETHEREUM_READ_WRITE_ABI} from '~/services/constants/ethereum_read_write'

export const state = () => ({
  contract: {
    instance: null,
    network: null,
  }
});

export const getters = {
  getContract: state => state.contract,
};

export const mutations = {
  registerEthereumReadWriteContractNetwork (state, payload) {
    console.log('ETHEREUM_READ_WRITE_STORE: registerEthereumReadWriteContractNetwork mutation: ', payload);
    state.contract.network = payload
  },
  registerEthereumReadWriteContractInstance (state, payload) {
    console.log('ETHEREUM_READ_WRITE_STORE: registerEthereumReadWriteContractInstance mutation: ', payload);
    state.contract.instance = () => payload
  }
};

export const actions = {
  async getContractInstance ({commit}) {
    console.log('ETHEREUM_READ_WRITE_STORE: getContractInstance action')
    let getContract = new Promise(function (resolve, reject) {
      let web3 = new Web3(window.web3.currentProvider)
      let ethereumReadWriteContractInstance = new web3.eth.Contract(ETHEREUM_READ_WRITE_ABI, ETHEREUM_READ_WRITE_ADDRESS);

      resolve(ethereumReadWriteContractInstance)
    });

    commit('registerEthereumReadWriteContractNetwork', ETHEREUM_READ_WRITE_NETWORK);

    try {
      let result = await getContract;

      commit('registerEthereumReadWriteContractInstance', result);
    } catch(e) {
      // console.log(e);
    }
  }
}
